<?php
$buildingObj = (object)[
    'name' => 'Caswynn Building',
    'floors' => 8,
    'address' => (object)[
        'street' => 'Timog Ave.',
        'city' => 'Quezon City',
        'country' => 'Philippines'
    ]
];

class Building {
    private $name;
    private $floor;
    private $address;
    

    public function __construct($name, $floor, $address){
        $this->name = $name;
        $this->floor = $floor;
        $this->address = $address;
    }

    function getDetails(){
        return "The name of the building is $this->name has $this->floor floor/s located at $this->address <br/>";
    }

    // modifier / setter
    function setName($name){
        $this->name = $name;
    }
    // accessor / getter
    function getName(){
        return $this->name;
    }
}

// [SECTION] Inheritance and Polymorphism
class Condominium extends Building {

    public function __construct($name, $floor, $address){
        $this->name = $name;
        $this->floor = $floor;
        $this->address = $address;
    }
    
    function getDetails(){
        return "The name of the condominium is $this->name it has $this->floor floor/s located at $this->address";
    }
}