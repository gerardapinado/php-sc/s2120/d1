<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S3: Classes and Objects</title>
</head>
<body>
    <h1>Objects from a Variable</h1>
    <h4>Normal Object</h4>
    <p><?= $buildingObj->name ?></p>
    <?php
        echo "<h4> Using Class and Object </h4>";
        // $bldg1 = new Building();
        // $bldg1->setName("Capitol Building");
        // echo $bldg1->getName();

        echo "<h4> Using Class Consturctor </h4>";
        // $bldg2 = new Building("Mayon Building");
        // echo $bldg2->getName();

        $bldg3 = new Building("Capitol Building", 3, "Albay");
        echo $bldg3->getDetails();

        echo "<h4> Using Class Inheritance and Polymorphism </h4>";
        $condo1 = new Condominium("Enzo Condo", 5, "Makati City");
        echo $condo1->getDetails();

    ?>
</body>
</html>